import numpy as np

import config as cfg
from database import handler_database as db
from feature_extractors import single_image_extraction as feature_extract
from models import unsupervised_knn as knn


def train_from_images_all_models(metric, model_architecture):
    conn = db.connect(cfg.db_name)
    if model_architecture.lower() == 'resnet':
        all_images = db.execute_query(conn, cfg.sql_get_images_resnet)
    elif model_architecture.lower() == 'vgg16':
        all_images = db.execute_query(conn, cfg.sql_get_images_vgg16)
    else:
        all_images = db.execute_query(conn, cfg.sql_get_images_vgg19)

    images_feature_vector = [np.fromstring(c[3].replace('[', '').replace(']', '')
                                           , dtype=float, sep=',') for c in all_images]

    # Train KNN model
    model = knn.train_model(training_set=images_feature_vector
                            ,algorithm=cfg.knn_algorithm
                            ,metric=metric
                            ,n_neighbours=7)
    # return model
    return model


def train_from_images_with_data(metric,neighbours, train_data_feature_vector):
    images_feature_vector = train_data_feature_vector
    # Train KNN model
    algorithm=cfg.knn_algorithm
    if metric != 'cosine' and metric != 'correlation':
        algorithm = 'ball_tree'

    model = knn.train_model(training_set=images_feature_vector
                            ,algorithm=algorithm
                            ,metric=metric
                            ,n_neighbours=neighbours)
    # return model
    return model


def get_neighbours_with_fe(img_path, model_architecture,metric):
    model=train_from_images_all_models(model_architecture=model_architecture,metric=metric)
    # Extract features of new image
    features = feature_extract.get_image_features(img_path=img_path,model_architecture=model_architecture)
    # Get values for image
    distances, indices = knn.get_neighbours(model
                                     , target_case=features)
    return distances, indices


def get_neighbours_from_vector(knn_trained_model, feature_vector):
    # Extract features of new image
    features = feature_vector
    # Get values for image
    distances, indices = knn.get_neighbours(knn_trained_model
                                     , target_case=features)
    return distances, indices