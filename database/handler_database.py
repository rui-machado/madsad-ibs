import sqlite3
import sys
import config as cfg

# Connect to Image index database
def connect(db_name):
    conn = sqlite3.connect(db_name)
    return conn


def execute_query(conn, query):
    c = conn.cursor()
    result=None
    # Insert a row of data
    try:
        result = c.execute(query).fetchall()
    except:
        print("Unexpected error:{0}".format(sys.exc_info()[0]))
        raise
    return result

'''
CREATE TABLE
'''

def create_table_images(conn, tb_name):
    c = conn.cursor()
    sql = 'create table if not exists ' + tb_name \
          + ' (cnn_model text' \
            ',cnn_layer text' \
            ',image_name text' \
            ',feature_vector text)'
    c.execute(sql)


def create_table_similarities(conn, tb_name):
    c = conn.cursor()
    sql = 'create table if not exists ' + tb_name \
          + ' (cnn_model text' \
            ',cnn_layer text' \
            ',input_image_name text' \
            ',nb_neighbours text' \
            ',neighbours text' \
            ',distances)'
    c.execute(sql)


def create_table_system_evaluation(conn, tb_name):
    c = conn.cursor()
    sql = 'create table if not exists {0}' \
         '(input_image_path text' \
         ',neighbour_image_path text' \
         ',input_category text'\
         ',input_sub_category text'\
         ',neighbour_category text'\
         ',neighbour_subcategory text'\
         ',cnn_model text'\
         ',distance_metric text'\
         ',distance float'\
         ',same_category text'\
         ',same_sub_category text'\
         ',color_histogram_correlation float'\
         ',texture_histogram_chisqr_distance float'\
         ',shape_correlation float)'.format(tb_name)
    c.execute(sql)


'''
INSERTS
'''


def insert_image(conn, data, tb_name):
    create_table_images(conn,tb_name)
    c = conn.cursor()
    # Insert a row of data
    try:
        c.execute("INSERT INTO {0} VALUES ('{1}','{2}','{3}','{4}')".format(tb_name
                                                                    , data[0]
                                                                    , data[1]
                                                                    , data[2]
                                                                    , data[3]))
        # Save (commit) the changes
        conn.commit()
    except:
        print("Unexpected error:{0}".format(sys.exc_info()[0]))
        raise


def insert_evaluation(conn,data):
    create_table_system_evaluation(conn, cfg.eval_tb_name)
    c = conn.cursor()
    # Insert a row of data
    '''
        '(input_image_path text' 
        'neighbour_image_path text' 
        'cnn_model text' 
        cat text
        subcat text
        cat text
        subcat text
        'distance_metric text' 
        'distance float' 
        'same_category text' 
        'same_sub_category text' 
        'color_histogram_correlation float' 
        'texture_histogram_correlation float' 
        'shape_correlation float)'.format(tb_name)
    '''
    try:
        c.execute("INSERT INTO {0} VALUES ('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',{9},'{10}','{11}',{12},{13},{14})"
                  .format(cfg.eval_tb_name
                          , data[0]
                          , data[1]
                          , data[2]
                          , data[3]
                          , data[4]
                          , data[5]
                          , data[6]
                          , data[7]
                          , data[8]
                          , data[9]
                          , data[10]
                          , data[11]
                          , data[12]
                          , data[13]
                          ))
        # Save (commit) the changes
        conn.commit()
    except:
        print("Unexpected error:{0}".format(sys.exc_info()[0]))
        raise

'''
MISC
'''

def print_all(conn, tb_name):
    c = conn.cursor()
    for row in c.execute('SELECT * FROM {0}'.format(tb_name)):
        print(row)

def get_single_img_path(conn, tb_name,model_architecture, img_index):
    c = conn.cursor()
    query = 'select cnn_model ,cnn_layer ,image_name,feature_vector' \
            ' from {0} where cnn_model=\'{1}\' and rank={2}'

    result = c.execute(query.format(tb_name,model_architecture,img_index))
    return result


def check_image_exists(conn, tb_name, img_path, model_name):
    c = conn.cursor()
    query = 'select 1 from {2} ' \
            'where cnn_model=\'{0}\' and image_name = \'{1}\' limit 1'.format(model_name,img_path,tb_name)
    result = c.execute(query)
    if result.fetchone() != None:
        return True
    return False

def get_images_from_table_by_model(conn, tb_name,model_name):
    c = conn.cursor()
    query = 'select * from {0} where cnn_model=\'{1}\''.format(tb_name,model_name)
    result = c.execute(query)
    if result.fetchone() != None:
        return result.fetchall()
    return None