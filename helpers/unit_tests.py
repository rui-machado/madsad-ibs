import sys
import time

from database import handler_database as db

db_name = '../database/jumia_images.db'
tb_name = 'static'

def test_db_creation():
    conn = db.connect(db_name)
    db.create_table_images(conn, tb_name)

def insert():
    # Test insert
    data = ['test', 'test', 'image path', '[[2.37689746e-05, 6.30830764e-05]]']
    conn = db.connect(db_name)
    db.insert_image(conn, data, tb_name)

start = time.time()
print("Creating database....")
try:
    test_db_creation()
    insert()
except:
    print("Unexpected error:{0}".format(sys.exc_info()[0]))
    raise
finally:
    end = time.time()
    print("FINISH Elapsed Time: {0}".format(end - start))


conn = db.connect(db_name)
db.print_all(conn,tb_name)


