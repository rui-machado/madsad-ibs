import os
import shutil

from flask import Flask, request, redirect
from werkzeug.utils import secure_filename

import config as cfg
from database import handler_database as db
from similarity import calculate_similarity as similarity

UPLOAD_FOLDER = cfg.upload_folder
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
base_html = '''
        <!doctype html>
        <html lang="en">
               <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
               <link href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">
               <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
               <script src="/static/scripters.js"></script>
               <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
               <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">

            <title>Upload new File</title>
            <body>
                <div class="container">
                    <div class="header">
                        <img width=150px src="/static/logo_fep.jpg"></img>
                        <h3>FEP-UP MADSAD</h3><br>
                        <h5>Image based search with CNN+Knn</h5><br>
                    </div>
                    <form method=post enctype=multipart/form-data>
                      <p>
                        <div class="input-group">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Browse… <input type="file" name=file style="display: none;" multiple="">
                            </span>
                        </label>
                        <input type="text" style="width:500px" class="form-control" readonly="">
                        </div><br>
                         <input class="btn btn-default" type=submit value=Search>
                    </form>

                    {0}

                    <p><br><footer class="footer">
                             <p>&copy; Rui Machado, FEP-UP, MADSAD (Porto,2017)</p>
                     </footer>
                </div>
            </body>
        </html>
        '''


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def get_image_neighbours(image_test,model_architecture,distance_metric):
    distances, indices = similarity.get_neighbours_with_fe(img_path=image_test
                                                           ,metric=distance_metric
                                                           , model_architecture=model_architecture)
    output = [distances, indices]
    return output


@app.route('/', methods=['GET', 'POST'])
def main():
    html_images = ''
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            #flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            #flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            temp_file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(temp_file_path)
            # Similarity calculation
            similar_images = get_image_neighbours(image_test=temp_file_path
                                                             ,model_architecture=cfg.model_web_app
                                                             ,distance_metric=cfg.model_metric)
            conn = db.connect(cfg.db_name)
            html_images += '<h4> Input image </h4>'
            html_images += '<img width=150px src=\'{0}\'></img><br><br>'\
                .format(os.path.join('/static/output/',filename))
            html_images += '<h4> Output images </h4>'
            html_images += '<div class="row">'
            for i in range(similar_images[0].size):
                img_idx = similar_images[1][0][i] + 1
                img_distance = similar_images[0][0][i]
                img = db.get_single_img_path(conn
                                       , model_architecture=cfg.model_web_app
                                       , img_index=img_idx
                                       , tb_name=cfg.train_tb_name).fetchone()
                # copy to temp folder
                fname = img[2].replace('\\','/').split('/')[len(img[2].replace('\\','/').split('/'))-1]
                new_file_name = '{0}/{1}'.format(cfg.upload_folder,fname)
                html_file_name = '/static/output/{0}'.format(fname)
                shutil.copy(img[2],new_file_name)
                # Added to webapp
                html_images += '<div class="col-sm-3"'
                html_images += '<h6>Index: {0}\nDistance: {1}\n<h6/>'.format(img_idx, img_distance)
                html_images += '<img width=150px src=\'{0}\'></img>'.format(html_file_name)
                html_images += '</div>'
            html_images+='</div>'
            return base_html.format(html_images)

    return base_html.format(html_images)


if __name__ == "__main__":
    app.run()