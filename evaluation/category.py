def get_subcategory(img_path):
    path_tree_img1 = img_path.replace('\\', '/').split('/')
    sub_family_1 = path_tree_img1[len(path_tree_img1) - 2]
    if sub_family_1.lower() == 'sweatshirts':
        sub_family_1='Sweatshirt'
    return sub_family_1

def get_category(img_path):
    path_tree_img1 = img_path.replace('\\', '/').split('/')
    family_1 = path_tree_img1[len(path_tree_img1) - 3]
    if family_1 == 'IBS Test Data' or family_1 == 'IBS Training Data' or family_1 == 'IBS Train Data':
        family_1 = path_tree_img1[len(path_tree_img1) - 2]
    return family_1

def check_same_category(img_path_1, img_path_2, index):
    # index= 0 => category
    # index= 1 => sub-category
    path_tree_img1=img_path_1.replace('\\','/').split('/')
    sub_family_1= path_tree_img1[len(path_tree_img1)-2]
    path_tree_img2=img_path_2.replace('\\','/').split('/')
    sub_family_2= path_tree_img2[len(path_tree_img2)-2]
    if sub_family_2.lower() == 'sweatshirts':
        sub_family_2='Sweatshirt'

    if index==0:
        family_1 = path_tree_img1[len(path_tree_img1) - 3]
        family_2 = path_tree_img2[len(path_tree_img2) - 3]
        if family_1 == 'IBS Test Data' or family_1 == 'IBS Training Data' or family_1=='IBS Train Data':
            family_1 = sub_family_1
        if family_2 == 'IBS Test Data' or family_2 == 'IBS Training Data' or family_1=='IBS Train Data':
            family_2 = sub_family_2
        # Unisex category handling
        if (sub_family_2.lower() == 'pants' or sub_family_2.lower()=='shoes'):
            return sub_family_1 == sub_family_2
        return family_1==family_2
    return sub_family_1 == sub_family_2