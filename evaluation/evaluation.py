import time


import numpy as np

import config as cfg
from database import handler_database as db
from evaluation import category
from evaluation import descriptors
from similarity import calculate_similarity as similarity

np.random.seed(12345)

def __get_train_images(model_name):
    conn=db.connect(cfg.db_name)
    results=db.get_images_from_table_by_model(conn,model_name=model_name,tb_name=cfg.train_tb_name)
    return results
def __get_test_images(model_name):
    conn = db.connect(cfg.db_name)
    results=db.get_images_from_table_by_model(conn, model_name=model_name, tb_name=cfg.test_tb_name)
    return results

def get_test_image_neighbours_analysis(model_name):
    train_data=__get_train_images(model_name)
    test_data=__get_test_images(model_name)

    ''' DECPRECATED => NOW INSERT INTO DB DIRECTLY
    output = []
    output.append(['input_image_path'
                      ,'neighbour_image_path'
                      ,'cnn_model'
                      ,'distance_metric'
                      ,'distance'
                      ,'same_category'
                      ,'same_sub_category'
                      ,'color_histogram_correlation'
                      ,'texture_histogram_correlation'
                      ,'shape_correlation'
                   ])
    '''
    # Extract features from database result
    train_feature_vector_list = [np.fromstring(c[3].replace('[', '').replace(']', '')
                                           , dtype=float, sep=',') for c in train_data]
    # train KNN model
    metrics=['euclidean','cosine','manhattan','chebyshev','minkowski','correlation']
    len_a = len(metrics)
    counter=1
    for metric in metrics:
        print('Processes evaluation for {2} on {0}/{1}'.format(counter, len_a, metric))
        knn_model = similarity.train_from_images_with_data(metric=metric
                                                           , neighbours=5
                                                           , train_data_feature_vector=train_feature_vector_list)
        # Similarity calculation
        test_images_vector = [np.fromstring(c[3].replace('[', '').replace(']', '')
                                           , dtype=float, sep=',') for c in test_data]
        test_images_path = [c[2] for c in test_data]
        # Get similar images

        for i in range(0,len(test_images_vector)):

            start_1 = time.time()
            neighbours=similarity.get_neighbours_from_vector(knn_trained_model=knn_model
                                                             ,feature_vector=test_images_vector[i])
            print('Images processed {0}/{1} in {2}'.format(i, len(test_images_vector),time.time()-start_1))
            start = time.time()
            # Get neighbour path from index
            for n in range(0, neighbours[0].size):
                neighbour_path = db.get_single_img_path(db.connect(cfg.db_name)
                                             , model_architecture=model_name
                                             , img_index=neighbours[1][0][n]
                                             , tb_name=cfg.train_tb_name_ranked).fetchone()
                # Build output
                # Model name, test_image_path, distance_metric,neighbour_path, neighbour distance
                db.insert_evaluation(db.connect(cfg.db_name)
                                  , [test_images_path[i]
                                  , neighbour_path[2]
                                  , category.get_category(test_images_path[i])
                                  , category.get_subcategory(test_images_path[i])
                                  , category.get_category(neighbour_path[2])
                                  , category.get_subcategory(neighbour_path[2])
                                  , model_name
                                  , metric
                                  , neighbours[0][0][n]
                                  , category.check_same_category(test_images_path[i], neighbour_path[2], 0)
                                  , category.check_same_category(test_images_path[i], neighbour_path[2], 1)
                                  , descriptors.color_evaluation_two_images(test_images_path[i], neighbour_path[2])
                                  , descriptors.texture_evaluation_two_images(test_images_path[i], neighbour_path[2])
                                  , descriptors.shape_evaluation_two_images(test_images_path[i], neighbour_path[2])])

        counter+=1

# print(check_same_category(img1,img2,0))

