import cv2
from skimage.feature import local_binary_pattern # To calculate a normalized histogram
from scipy.stats import itemfreq
from scipy.stats import spearmanr
import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

np.random.seed(12345)


def color_evaluation_two_images(img1,img2):
    '''
        USING COLOR HISTOGRAMS    
    '''
    # reading the images
    base = cv2.imread(img1, 1)
    test1 = cv2.imread(img2, 1)
    base=cv2.resize(base, (300, 300))
    test1 = cv2.resize(test1, (300, 300))

    hist_base = cv2.calcHist([base], [0, 1, 2],None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    hist_test = cv2.calcHist([test1], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])

    # Compare two Hist. and find out the correlation value
    base_test1 = cv2.compareHist(hist_base, hist_test,method=cv2.HISTCMP_CORREL)
    return base_test1


def texture_evaluation_two_images(img1,img2):
    '''
          USING LBP + HISTOGRAMS    
      '''
    # reading the images and convert them to grayscale

    base = cv2.imread(img1, 0)
    test1 = cv2.imread(img2, 0)

    newHeight = 224
    newWidth = 224

    dim = (newHeight, newWidth)
    base = cv2.resize(base, dim, interpolation=cv2.INTER_AREA)
    test1 = cv2.resize(test1, dim, interpolation=cv2.INTER_AREA)

    radius = 3
    no_points = 8 * radius

    #hist for img1
    lbp = local_binary_pattern(base, no_points, radius, method='uniform')
    #get histgram
    x = itemfreq(lbp.ravel())
    #normalize hist
    hist = x[:, 1] / sum(x[:, 1])

    #hist for img2
    lbp_test1 = local_binary_pattern(test1, no_points, radius, method='uniform')
    #get histgram
    y = itemfreq(lbp_test1.ravel())
    #normalize hist
    hist_test1 = y[:, 1] / sum(y[:, 1])

    #get correlation between histograms
    base_test1 = cv2.compareHist(np.array(hist, dtype=np.float32)
                                 , np.array(hist_test1, dtype=np.float32)
                                 , method=cv2.HISTCMP_CHISQR)

    #null hypothesis is that two sets of data are uncorrelated
    return base_test1


def shape_evaluation_two_images(img1,img2):
    '''
              USING HU MOMENTS + SPEARMAN CORRELATION
    '''
    # reading the images and convert them to grayscale
    base = cv2.imread(img1, 0)
    test = cv2.imread(img2, 0)

    newHeight = 300
    newWidth = 300

    dim = (newHeight, newWidth)
    base = cv2.resize(base, dim, interpolation=cv2.INTER_AREA)
    test = cv2.resize(test, dim, interpolation=cv2.INTER_AREA)


    huMoments_base=cv2.HuMoments(cv2.moments(base))
    huMoments_test=cv2.HuMoments(cv2.moments(test))
    return spearmanr(huMoments_base,huMoments_test)[0]


def show_images(img1,img2, colored):
    base = cv2.imread(img1,colored)
    test1 = cv2.imread(img2,colored)

    newHeight = 300
    newWidth = 300

    dim = (newHeight, newWidth)
    base = cv2.resize(base, dim, interpolation=cv2.INTER_AREA)
    test1 = cv2.resize(test1, dim, interpolation=cv2.INTER_AREA)

    cv2.imshow('image', base)
    cv2.imshow('image2', test1)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def show_texture_histogram(img1, img2, corr):
    img = cv2.imread(img1,1)
    img_comp = cv2.imread(img2,1)
    color = ('b', 'g', 'r')
    plt.figure(1)

    base = cv2.imread(img1, 0)
    test1 = cv2.imread(img2, 0)

    eps = 1e-7
    radius = 3
    no_points = 8 * radius
    #base image
    lbp = local_binary_pattern(base, no_points,
                                       radius, method="uniform")
    (hist, _) = np.histogram(lbp.ravel(),
                             bins=np.arange(0, no_points + 3),
                             range=(0, no_points + 2))
    # normalize the histogram
    hist = hist.astype("float")
    hist /= (hist.sum() + eps)
    #test image
    lbp2 = local_binary_pattern(test1, no_points,
                               radius, method="uniform")
    (hist2, _) = np.histogram(lbp2.ravel(),
                             bins=np.arange(0, no_points + 3),
                             range=(0, no_points + 2))
    # normalize the histogram
    hist2 = hist2.astype("float")
    hist2 /= (hist2.sum() + eps)

    #
    # plt.subplot(211)
    # plt.title('Visual texture histogram comparison using LBP - Chi squared distance:{0}'.format(round(corr, 2)))
    # plt.hist(hist)
    # plt.subplot(212)
    # plt.hist(hist2)
    # plt.show()

    # construct the figure
    plt.style.use("ggplot")
    fig = plt.figure()
    fig.suptitle('Visual texture histogram comparison using LBP - Chi squared distance:{0}'.format(round(corr, 2)))
    plt.ylabel("% of Pixels")
    plt.xlabel("LBP pixel bucket")
    n_bins = lbp.max() + 1
    plt.hist(lbp.ravel(), normed=True, bins=n_bins, range=(0, n_bins))
    plt.show()
    # plt.style.use("ggplot")
    # fig = plt.figure()
    # fig.suptitle('Visual texture histogram comparison using LBP - Chi squared distance:{0}'.format(round(corr, 2)))
    # plt.ylabel("% of Pixels")
    # plt.xlabel("LBP pixel bucket")
    # n_bins2 = lbp2.max() + 1
    # plt.hist(lbp2.ravel(), normed=True, bins=n_bins2, range=(0, n_bins2))
    # plt.show()

def show_color_histogram(img1, img2, corr):
    img = cv2.imread(img1,1)
    img_comp = cv2.imread(img2,1)
    color = ('b', 'g', 'r')
    plt.figure(1)

    for i, col in enumerate(color):
        histr = cv2.calcHist([img], [i], None, [256], [0, 256])
        histr2 = cv2.calcHist([img_comp], [i], None, [256], [0, 256])

        plt.subplot(211)
        plt.title('Visual histogram comparison - correlation:{0}'.format(round(corr, 2)))
        plt.plot(histr, color=col)
        plt.subplot(212)
        plt.plot(histr2, color=col)
        plt.xlim([0, 256])
    plt.show()