from evaluation import evaluation as eval



vgg16_neighbours=eval.get_test_image_neighbours_analysis('Vgg16')
vgg19_neighbours=eval.get_test_image_neighbours_analysis('Vgg19')
resnet_neighbours=eval.get_test_image_neighbours_analysis('Resnet50')

'''
# Write to file
with open('evaluation/ibs_neighbours_analysis.csv', 'w') as fp:
    fp.truncate()
    spamwriter = csv.writer(fp, delimiter=';')
    for row in vgg16_neighbours:
        spamwriter.writerow(row)
    for row in vgg19_neighbours:
        spamwriter.writerow(row)
    for row in resnet_neighbours:
        spamwriter.writerow(row)
'''
