from evaluation import descriptors
from evaluation import evaluation as eval
from scipy.stats import spearmanr

img1 = './static/Demos/a.png'
img_dummy = './static/Demos/fep.jpg'
img_dummy_2 = './static/Demos/fep2.jpg'
img2 = './static/Demos/b.png'

def func_texture(img1, img2):
    descriptors.show_images(img1,img2,1)
    res=descriptors.texture_evaluation_two_images(img1,img2)
    descriptors.show_texture_histogram(img1, img2,res)
    print(res)

func_texture(img1, img2)