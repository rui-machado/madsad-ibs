import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import datetime
import plotly.plotly as py
import numpy as np
import pandas as pd
from sklearn import preprocessing
from tabulate import tabulate

# To be able to run in python console
import os
os.chdir('C:\\Users\\Utilizador\\PycharmProjects\\madsad-ibs')
plt.interactive(False)


'''
    HELPERS 
'''

# create a new distance column normalized
def maxmin_normalize_column(df, target_col, new_col_name):
    df[new_col_name] = df[target_col]
    min_max_scaler = preprocessing.MinMaxScaler()
    temp_df_data=[]
    # standardize distance
    for model in df_data['cnn_model'].unique():
        for metric in df[df['cnn_model']==model]['distance_metric'].unique():
            df_norm = df[(df['cnn_model']==model) & (df['distance_metric']==metric)]
            df_norm[new_col_name] = min_max_scaler.fit_transform(df_norm[target_col])
            temp_df_data.append(df_norm)
    return pd.concat(temp_df_data)

# Read file with model results
df_data=pd.read_csv('analysis/ibs_neighbours_analysis.csv', sep=';',decimal='.')

# Analyze the dataset
df_data.describe()

#clean multiple header rows
df_data=df_data[df_data['cnn_model']!='cnn_model']

# Change data types
df_data['distance']=df_data['distance'].astype(float)

# normalize columns
df_data=maxmin_normalize_column(df_data,'distance','distance_norm')
print(df_data.columns)
'''
    DESCRIPTIVE STATISTICS
'''

stats=df_data[['cnn_model','distance_metric','distance_norm','same_category','same_sub_category']]\
    .sort_values(by=['cnn_model'],ascending=[1])\
    .groupby(['distance_metric','cnn_model','same_category','same_sub_category'])\
    .agg(['mean','count'])

print(tabulate(stats))
stats.plot(kind='bar')
plt.show()
'''

'''