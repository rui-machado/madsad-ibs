drop table final_system_evaluation_ranked
create table final_system_evaluation_ranked as
select *,(	
			SELECT COUNT(*)+1 
			FROM (final_system_evaluation) inter
			WHERE (case when distance_metric in ('manhattan','minkowski','euclidean') then inter.distance < ibs.distance else inter.distance > ibs.distance end)
				and inter.input_image_path=ibs.input_image_path
				and inter.cnn_model=ibs.cnn_model
				and inter.distance_metric=ibs.distance_metric 							
			) as rank
from (final_system_evaluation) ibs
order by input_image_path desc

