update final_system_evaluation 
set neighbour_image_path= replace(neighbour_image_path,'\','/') 

update final_system_evaluation 
set input_image_path= replace(input_image_path,'\','/') 

drop table final_system_evaluation_agg
create table final_system_evaluation_agg as
select *, case when instr(sub_category,'\')=0 then category else substr(sub_category,1,instr(sub_category,'\')-1) end as sub_cat
	from (
		select ibs.*,
			case when same_category='True' then 1 else 0 end as correct_category,
			case when same_category='False' then 1 else 0 end as wrong_category,
			case when same_sub_category='True' then 1 else 0 end as correct_sub_category,
			case when same_sub_category='False' then 1 else 0 end as wrong_sub_category,
			replace(input_image_path,'./static/Train_test_images/IBS Test Data/','') as input_image_path,
			substr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/',''), 1, instr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/',''), '\') - 1) AS category,
			substr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/','')
				, instr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/',''),'\')+1
				, length(replace(input_image_path,'./static/Train_test_images/IBS Test Data/','')) - 1) AS sub_category,
			cast((
				select  count(*)+1
				from    ibs_neighbours_analysis as p2
				where   (case when p2.distance_metric in ('manhattan','euclidean','minkowski') then p2.distance < ibs.distance else p2.distance > ibs.distance end)
					and p2.input_image_path=ibs.input_image_path
					and p2.distance_metric=ibs.distance_metric
					and p2.cnn_model=ibs.cnn_model
			) as string) as rank
		from ibs_neighbours_analysis as ibs			
	) b	
											   

