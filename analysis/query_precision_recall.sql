select 
	distance_metric
	, cnn_model
	, input_image_path
	,round((((select count(*)+1 from final_system_evaluation_ranked a where a.rowid>b.rowid and same_sub_category='True' and distance_metric=b.distance_metric and cnn_model=b.cnn_model)+0.0)
		/ 
	((select count(*)+1 from final_system_evaluation_ranked a where a.rowid>b.rowid and distance_metric=b.distance_metric and cnn_model=b.cnn_model)+0.0)),3)	as precision
	 
	,round((((select count(*)+1 from final_system_evaluation_ranked a where a.rowid>b.rowid and same_sub_category='True' and distance_metric=b.distance_metric and cnn_model=b.cnn_model)+0.0)
		/ 
	((select count(*)+1 from final_system_evaluation_ranked a where same_sub_category='True' and distance_metric=b.distance_metric and cnn_model=b.cnn_model)+0.0)),3)	as recall
	
from final_system_evaluation_ranked as b
where cnn_model='Vgg16'
and distance_metric='correlation'
group by input_image_path
	,distance_metric
	, cnn_model
order by rowid desc


