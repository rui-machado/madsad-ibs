create table final_jumia_images_train_ranked as
select cnn_model ,cnn_layer ,image_name,feature_vector,(	
			SELECT COUNT(*)+1 
			FROM final_jumia_images_train inter
			WHERE inter.rowid < ibs.rowid 
				and inter.cnn_model=ibs.cnn_model
				and inter.cnn_layer=ibs.cnn_layer 							
			) as rank
from final_jumia_images_train ibs
order by image_name desc




