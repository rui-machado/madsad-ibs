-- select * from ibs_neighbours_analysis
-- select * from ibs_results
--delete from ibs_neighbours_analysis  where cnn_model = 'cnn_model' or cnn_model is null

drop table if exists ibs_results;
create table ibs_results as
select cnn_model,
	distance_metric,
	rank,
	category ,
	(case when sub_cat == 'Sweatshirts' then 'Sweatshirt' else sub_cat end) as sub_category,
	sum(correct_category) as correct_cat,
	sum(correct_sub_category) as correct_sub_cats,
	count(*) as nb_cases
from(
	select *, case when instr(sub_category,'\')=0 then category else substr(sub_category,1,instr(sub_category,'\')-1) end as sub_cat
	from (
		select ibs.*,
			case when same_category='True' then 1 else 0 end as correct_category,
			case when same_category='False' then 1 else 0 end as wrong_category,
			case when same_sub_category='True' then 1 else 0 end as correct_sub_category,
			case when same_sub_category='False' then 1 else 0 end as wrong_sub_category,
			replace(input_image_path,'./static/Train_test_images/IBS Test Data/','') as input_image_path,
			substr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/',''), 1, instr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/',''), '\') - 1) AS category,
			substr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/','')
				, instr(replace(input_image_path,'./static/Train_test_images/IBS Test Data/',''),'\')+1
				, length(replace(input_image_path,'./static/Train_test_images/IBS Test Data/','')) - 1) AS sub_category,
			cast((
				select  count(*)+1
				from    ibs_neighbours_analysis as p2
				where   (case when p2.distance_metric in ('manhattan','euclidean','minkowski') then p2.distance < ibs.distance else p2.distance > ibs.distance end)
					and p2.input_image_path=ibs.input_image_path
					and p2.distance_metric=ibs.distance_metric
					and p2.cnn_model=ibs.cnn_model
			) as string) as rank
		from ibs_neighbours_analysis as ibs			
	) b	
) a
group by cnn_model,distance_metric, rank, (case when sub_cat == 'Sweatshirts' then 'Sweatshirt' else sub_cat end), sub_cat;
select * from ibs_results;