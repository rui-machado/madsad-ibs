import time

import config as cfg
import feature_extractors.extractorResNet as resnet
import feature_extractors.extractorVGG16 as vgg16
import feature_extractors.extractorVGG19 as vgg19
from database import handler_database as db
from helpers import handler_filesystem as file


# Feature extractor
def extract_with_model(img, layer_to_extract,model, model_name):
    #check if image is in DB
    conn = db.connect(cfg.db_name)
    img_exists=db.check_image_exists(conn,cfg.train_tb_name,img,model_name)
    if img_exists==False:
        # Extract visual features
        features = model.get_image_features(img)
        # Save into database
        data = [model_name, layer_to_extract, img, '{0}'.format(features.tolist())]

        db.insert_image(conn, data, cfg.train_tb_name)
    else:
        print("Image {0} already in database for model {1}".format(img, model_name))

# Main routine to launch feature extraction and store in DB
def run():
    start = time.time()
    # Initialize models
    layer_to_extract_vgg = "fc2"
    layer_to_extract_resnet = "fc1000"
    print("Initializing models...")
    model_resnet = resnet.ExtractorResNet(layer_to_extract_resnet
                                          ,"model_weights/resnet50_weights_th_dim_ordering_th_kernels.h5")
    model_vgg16 = vgg16.ExtractorVGG16(layer_to_extract_vgg
                                       ,"model_weights/vgg16_weights_th_dim_ordering_th_kernels.h5")
    model_vgg19 = vgg19.ExtractorVGG19(layer_to_extract_vgg
                                       ,"model_weights/vgg19_weights_th_dim_ordering_th_kernels.h5")

    print("Extracting features models...")
    # Get images and extract features and store to db
    all_images = file.get_files(cfg.train_images_folder)
    nb_images = len(all_images)
    counter=0
    for img in all_images:
        # for each image extract features and save to database
        extract_with_model(img, layer_to_extract_resnet, model_resnet, 'Resnet50')
        extract_with_model(img, layer_to_extract_vgg, model_vgg16,'Vgg16')
        extract_with_model(img, layer_to_extract_vgg, model_vgg19,'Vgg19')
        counter=counter + 1
        print("Extracted {0}/{1}".format(counter,nb_images))
    print("Process took {0} seconds.".format(time.time()-start))

# Run main
if __name__ == "__main__":
    run()

