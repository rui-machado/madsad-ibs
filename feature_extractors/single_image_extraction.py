import feature_extractors.extractorVGG19 as vgg19
import feature_extractors.extractorVGG16 as vgg16
import feature_extractors.extractorResNet as resnet


# Feature extractor
def __extract_with_model(img,model):
    # Extract visual features
    features = model.get_image_features(img)
    return features

# Main routine to launch feature extraction and store in DB
def get_image_features(img_path, model_architecture):
    # Initialize models
    layer_to_extract_vgg = "fc2"
    layer_to_extract_resnet = "fc1000"
    result = None
    if model_architecture.lower() == 'resnet':
        model_resnet = resnet.ExtractorResNet(layer_to_extract_resnet
                                              ,"model_weights/resnet50_weights_th_dim_ordering_th_kernels.h5")
        result = __extract_with_model(img_path, model_resnet)
    elif model_architecture.lower() == 'vgg16':
        model_vgg16 = vgg16.ExtractorVGG16(layer_to_extract_vgg
                                           ,"model_weights/vgg16_weights_th_dim_ordering_th_kernels.h5")
        result = __extract_with_model(img_path, model_vgg16)
    else:
        model_vgg19 = vgg19.ExtractorVGG19(layer_to_extract_vgg
                                       ,"model_weights/vgg19_weights_th_dim_ordering_th_kernels.h5")
        result = __extract_with_model(img_path, model_vgg19)
    return result



