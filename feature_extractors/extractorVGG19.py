from models.vgg19 import VGG19
from keras.applications.vgg19 import preprocess_input
from keras.preprocessing import image
from keras.models import Model
import numpy as np


class ExtractorVGG19:
    # Class attributes
    base_model = None
    model = None

    def __init__(self, extraction_layer, weights_path):
        # Initialize VGG19 model with weights from ImageNet
        self.base_model = VGG19(weights_path=weights_path)
        # Define model
        self.model = Model(input=self.base_model.input, output=self.base_model.get_layer(extraction_layer).output)

    def get_image_features(self,img_path):
        # Load image and reset size to respect ImageNet constraints
        img = image.load_img(img_path, target_size=(224, 224))
        # Convert image to array
        img = image.img_to_array(img)
        # Extract features
        img = np.expand_dims(img, axis=0)
        # Preprocess using Keras model constraints (Each available model has a different pre process)
        img = preprocess_input(img)
        # Return feature object
        return self.model.predict(img)


