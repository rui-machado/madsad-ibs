import cgi, cgitb

cgitb.enable()
form = cgi.FieldStorage()
filedata = form['upload']

if filedata.file: # field really is an upload
    with open("test.jpg", 'w') as outfile:
        outfile.write(filedata.file.read())