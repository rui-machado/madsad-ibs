from sklearn.neighbors import NearestNeighbors

# Valid values for metric are:
#
# - from scikit-learn: ['cityblock', 'cosine', 'euclidean', 'l1', 'l2','manhattan']
#
# - from scipy.spatial.distance: ['braycurtis', 'canberra', 'chebyshev',
#   'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski',
#    'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto',
#    'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath',
#    'sqeuclidean', 'yule']

def train_model(training_set, n_neighbours, algorithm, metric):
    # Train NearestNeighbours with algorithm and metric
    model = NearestNeighbors(n_neighbors=n_neighbours
                            , algorithm=algorithm
                            , metric=metric).fit(training_set)
    return model


def get_neighbours(model, target_case):
    # Apply model to target case
    distances, indices = model.kneighbors(target_case)
    # return values
    return distances, indices

